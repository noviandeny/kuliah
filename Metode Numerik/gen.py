#!/usr/bin/python2

import sys
def saveToFile(j):
    res = ""
    for i in range(10001):
        res += "{} {}\n".format(i*dX,arr[i][j])

    open("points/type1/{}.dat".format(j),'w').write(res[:-1])

dX = 0.001
arr = [[0 for i in range(10001)] for j in range(10001)]

for i in range(10001):
    x = float(i*dX)
    arr[i][0] = (-x**2 + 10*x)**0.5
    arr[i][1] = arr[i][0]-0.001

arr[0][1],arr[10000][1] = 0,0

for j in range(1,10000):
    for i in range(1,9999):
        arr[i][j+1] = arr[i+1][j] + arr[i-1][j] - arr[i][j-1]

for i in range(10001):
    saveToFile(i)
    sys.stdout.write('\r{}'.format(i))
    sys.stdout.flush()
