#!/usr/bin/python2

import sys,os
def saveToFile(j):
    res = ""
    for i in range(10001):
        res += "{} {}\n".format(i*dX,arr[i][j])

    open("points/type2/{}.dat".format(j),'w').write(res[:-1])

dX = 0.001
arr = [[0 for i in range(10001)] for j in range(10001)]

for i in range(5000):
    x = float(i*dX)
    arr[i][0] = x
    arr[i][1] = x

for i in range(5000,10001):
    x = float(i*dX)
    arr[i][0] = -x + 10
    arr[i][1] = -x + 10

arr[5000][1] -= 0.001

for j in range(1,10000):
    for i in range(1,9999):
        arr[i][j+1] = arr[i+1][j] + arr[i-1][j] - arr[i][j-1]

for i in range(10001):
    saveToFile(i)
    sys.stdout.write('\r{}'.format(i))
    sys.stdout.flush()
