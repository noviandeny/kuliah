#!/usr/bin/python2
import sys

# f(x) = x**3 + 2*x**2 + x + 3
# f'(x) = 3*x**2 + 4*x + 1

def f(x):
    return x**3+2*x**2 + x +3

def f_(x):
    return 3*x**2 +4*x +1

def main(x1,x2):
    if (f_(x1) * f_(x2)) < 0:
        if (f_(x1) >= 0):
            x1,x2 = x2,x1
        while(abs(f_(x1))>0.0001):
            print "\n\tx1,x2 = {},{}".format(x1,x2)
            x3 = x1+(x2-x1)/2
            print '\n\tx3 = {}\n\tf\'(x3) = {}'.format(x3,f_(x3))
            if(f_(x3)<0):
                print '\n\tReplace x1'
                x1 = x3
            else:
                print '\n\tReplace x2'
                x2 = x3
        print "Titik optimum = ({},{})".format(x1,f(x1))

    else:
        print "Turunan x1 dan x2 harus beda tanda"


if __name__ == '__main__':
    if (len(sys.argv) != 3) :
        print "[+] Usage : \n program_name x1 x2"
    else:
        try:
            x1 = float(sys.argv[1])
            x2 = float(sys.argv[2])
            main(x1,x2)
        except:
            print 'Invalid points'
